describe('Automating PlanDay schedule page', function(){
    it("Open PlanDay, schedule page, assertions",function ()  {
        cy.visit('https://test1234.planday.com/')
        cy.contains('OK').click()
        cy.contains('Username (your email)')
                .type('plandayqa@outlok.com')
        cy.wait(1000)
        cy.contains('Password')
                .type('APItesting21')
        cy.wait(1000)
        cy.contains('Log in').click()
        cy.wait(1000)
        cy.get('iframe')
        .first()
        .its('0.contentDocument.body')
        .should('not.be.undefined')
        .and('not.be.empty')
        .then(cy.wrap)
        .find('#recaptcha-anchor')
        .should('be.visible')
        .click();
        cy.visit('https://test1234.planday.com/page/home')
        cy.contains('Schedule').click
        cy.visit('https://test1234.planday.com/page/schedule')
        cy.url()
                 .should('include','/schedule')
        cy.contains('Employee')
        cy.get('Employee')
                 .should('have.length', 3);
    });
})