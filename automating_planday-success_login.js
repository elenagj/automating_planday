describe('Automating PlanDay log in success', function(){
    it("Open PlanDay, assertions, type credentials, log in success",function ()  {
        cy.visit('https://test1234.planday.com/')
        cy.url()
                .should('include','/Login')
        cy.contains('OK').click()
        cy.contains('Username (your email)')
                .type('plandayqa@outlok.com')
        cy.wait(1000)
        cy.contains('Password')
                .type('APItesting21')
        cy.wait(1000)
        cy.contains('Log in').click()
        cy.wait(1000)
        cy.get('iframe')
        .first()
        .its('0.contentDocument.body')
        .should('not.be.undefined')
        .and('not.be.empty')
        .then(cy.wrap)
        .find('#recaptcha-anchor')
        .should('be.visible')
        .click();
        cy.visit({
          url: 'https://test1234.planday.com/page/home',
        method: 'POST',
        body:{
          Username: 'plandayqa@outlook.com',
          Password: 'APItesting21',
          ReturnUrl: '/connect/authorize/callback?client_id=b116846e-8ff0-42dc-83b6-5392543ca73c&redirect_uri=https%3A%2F%2Ftest1234.planday.com%2Fauth-callback&response_type=code&scope=openid%20impersonate%20plandayid&state=c14515d8b6f342ffbba94e9fdb4a7da6&code_challenge=GLhMTtqw4ynK2kz4eLBMCnljOVj7tDoscDSJxB1eBhw&code_challenge_method=S256&acr_values=tenant%3Atest1234.planday.com&response_mode=query',
          TenantSpecified: 'True',
          PortalAlias: 'test1234.planday.com',
          LoginStep: 'PasswordValidation',
          RequestVerificationToken: 'CfDJ8DtYzL0A_L5IvOZPAmWSK23DaIQima7MXIVZfYmB5ppW_DhJqWMgWYOTP_alTGd7rOFyaLlZveO5KSnrkAZ5FW8V-BpRUzwA7q8yylcQXuzn5JVdOPJiK_qIvDkIw6o1aVi_QIuyw8PVPh4sSKnGxuc'
        },
    })
    });
})