describe('Automating PlanDay log in fail', function(){
    it("Open PlanDay, assertions, type invalid credentials, log in fail",function ()  {
        cy.visit('https://test1234.planday.com/')
        cy.url()
                .should('include','/Login')
        cy.contains('OK').click()
        cy.contains('Username (your email)')
                .type('planday@outlok.com')
        cy.contains('Password')
                .type('APItesting')
        cy.contains('Log in').click()
        Cypress.Commands.add('confirmCaptcha', function () {
            cy.get('iframe')
              .first()
              .then((recaptchaIframe) => {
                const body = recaptchaIframe.contents()
                cy.wrap(body).find('.recaptcha-checkbox-border').should('be.visible').click()
              })
          })
        cy.contains('The username or password is incorrect.')
    });
})